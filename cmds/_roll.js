var getRandomInt = require(__dirname + '/../libs/getRoll')
module.exports = function (_args) {
  var roll_debug = require('debug')('roll')
  let roll_string = _args
  let dice_stats = {}
  let splitter_opts = []
  roll_debug('roll_string', roll_string)

  ;['#', 'd', '+'].forEach((splitter) => {
    if (_args.indexOf(splitter) > -1) {
      splitter_opts.push(splitter)
    }
  })

  ;splitter_opts.forEach((splitter, index, array) => {
    let _args_split = _args.split(splitter)
    roll_debug('splitter', splitter)
    roll_debug('_args_split', _args_split)
    roll_debug('_args_split.length', _args_split.length)
    if (_args_split.length > 1) {
      dice_stats[splitter] = parseInt(_args_split[0])
      _args = _args_split[1]
      if (index === array.length - 1)dice_stats['last'] = _args_split[1] || 0
    }
  })

  ;['#', 'd', '+', 'last'].forEach((splitter) => {
    if (dice_stats[splitter]) dice_stats[splitter] = parseInt(dice_stats[splitter])
  })

  roll_debug('dice_stats', dice_stats)
  if (dice_stats['+'] < 1) return 'Incorrect dice format'

  let dice_rolls = []
  let dice_roll_total = 0
  function sub_dice_roll () {
    let sub_dice_rolls = []
    let sub_dice_roll_total = 0
    for (let j = dice_stats['d'] || 1;j > 0;j--) {
      let dice_max = dice_stats['+'] || dice_stats['last']
      let roll = getRandomInt(1, dice_max)
      roll_debug('roll', roll)
      sub_dice_rolls.push(roll)
      sub_dice_roll_total += roll
    }
    if (dice_stats['+']) {
      sub_dice_roll_total += dice_stats['last']
    }
    dice_rolls.push(sub_dice_rolls)
    dice_roll_total += sub_dice_roll_total
  }

  if (dice_stats['#']) {
    for (let i = dice_stats['#'] || 1;i > 0;i--) {
      sub_dice_roll()
    }
  }else {
    sub_dice_roll()
  }
  roll_debug('dice_rolls', dice_rolls)
  return `Dice rolled! ${roll_string} : ${dice_roll_total}`
}
