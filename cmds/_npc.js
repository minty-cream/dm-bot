/* `!npc [name] ( [related to another NPC] || [from location] || [owns poi]) - this automatically create an entry in the database as well as any relations to any other NPCs, existing or non-existant, and their current place of residence. Or any places of interest they own.*/

var _split_args = require(__dirname + '/../libs/splitArgs')
var db = require(__dirname + '/../libs/db')
var debug = require('debug')('_npc')

module.exports = function (_args) {
  let _splitters = ['related to', 'from', 'owns']
  let _triple = _split_args(_splitters, _args)
  let _out_string = `Gotcha, an npc named ${_triple.subject} is ${_triple.predicate} ${_triple.object}`
  // TODO Replace Gotcha, with the username of the person who called it
  debug('out_string', _out_string)
  db.put(_triple, (err) => {
    if (err) {
      let _err_string = 'Error: ' + err
      debug(_err_string, err)
      return _err_string
    }
    debug('Succesful npc entry', _triple)
  })
  return _out_string
}
