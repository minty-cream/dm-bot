/* `!npc [name] ( [related to another NPC] || [from location] || [owns poi]) - this automatically create an entry in the database as well as any relations to any other NPCs, existing or non-existant, and their current place of residence. Or any places of interest they own.*/

var _split_args = require(__dirname + '/../libs/splitArgs')
var db = require(__dirname + '/../libs/db')
var debug = require('debug')('_describe')

module.exports = function (_args) {
  debug('_args', _args)
  if (_args.indexOf(':') > -1) {
    let _splitters = [':']
    let _triple = _split_args(_splitters, _args)
    let _out_string = `Gotcha, ${_triple.subject} is described!`
    // TODO Replace Gotcha, with the username of the person who called it
    debug('out_string', _out_string)
    db.put(_triple, (err) => {
      if (err) {
        let _err_string = 'Error: ' + err
        debug(_err_string, err)
        return _err_string
      }
      debug('Succesful description entry', _triple)
    })
    return _out_string
  }else {
    db.get({subject: _args}, (err, list) => {
      if (err) {
        let _err_string = 'Error: ' + err
        debug(_err_string, err)
        return _err_string
      }
      if (process.env.ENVIRONMENT === 'production' || process.env.ENVIRONMENT === 'staging') {
        // db.message etc
      }else {
        console.log('dm-bot says <> ', list)
      }
    })
    return 'Give me a moment...'
  }
}
