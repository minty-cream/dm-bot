/** What does a DM bot need?
 *
 * `!roll Num#XdY+Z  - get a result in return
 * `!npc [name] [related to another NPC] [from location] [owns poi] - this automatically create an entry in the database as well as any relations to any other NPCs, existing or non-existant, and their current place of residence. Or any places of interest they own.
 * `!describe [name] : [description] - This describes any place, item, or NPC 
 * `!describe [name] - bot spouts out all the infromation it knows about a place, including its description, relationships, and points of interest`
 * `!place [name] [leads to another place]- this automatically creates an entry in the database as well as any roads to any other places, existing or non-existant
 * `!poi [name] [is in place] [belongs to NPC] - creates an entry in the DB, including any non-existant or existing NPC's or Places'
 * `!item [name] : [description] - Alias for describe
 **/
module.exports = {
  '!describe': require(__dirname + '/_describe'),
  '!item': function (_args) {},
  '!npc': require(__dirname + '/_npc'),
  '!place': function (_args) {},
  '!poi': function (_args) {},
  '!roll': require(__dirname + '/_roll')
}
