var botCmds = require(__dirname + '/cmds')

module.exports = function () {
  console.log('!roll d6 test', botCmds['!roll']('d6'))
  console.log('!roll 3#1d6 test', botCmds['!roll']('3#d6'))
  console.log('!roll 3#1d6+3 test', botCmds['!roll']('3#d6+3'))
  console.log('!roll 1d6 test', botCmds['!roll']('1d6'))
  console.log('!roll 3#1d6 test', botCmds['!roll']('3#1d6'))
  console.log('!roll 1d6+3 test', botCmds['!roll']('1d6+3'))
  console.log('!roll 3#1d6+3 test', botCmds['!roll']('3#1d6+3'))
  console.log('!npc Thor related to Loki', botCmds['!npc']('Thor related to Loki'))
  console.log('!npc Thor from Yggdrasil', botCmds['!npc']('Thor from Yggdrasil'))
  console.log('!npc Thor owns Myljornir', botCmds['!npc']('Thor owns Myljornir'))
  console.log('!describe Thor : Long golden hair', botCmds['!describe']('Thor:Long golden hair'))
  console.log('!describe Thor', botCmds['!describe']('Thor'))
}
