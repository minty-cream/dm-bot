module.exports = function (min, max) {
  min = Math.ceil(parseInt(min))
  max = Math.floor(parseInt(max))
  return Math.floor(Math.random() * (max - min + 1)) + min
}
