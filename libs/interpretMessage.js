var botCmds = require(__dirname + '/../cmds')

module.exports = function (msg) {
  // console.log('msg incoming!', msg)
  console.log('msg incoming!')
  let _msg_i = msg.content.indexOf(' ')
  console.log('_msg_i', _msg_i)
  if (_msg_i > -1) {
    let cmd = msg.content.slice(0, _msg_i).trim()
    let args = msg.content.slice(_msg_i + 1).trim()
    console.log('cmd', cmd)
    console.log('args', args)
    console.log('run command', botCmds[cmd])
    return botCmds[cmd](args) || 'Command not found, or command input invalid'
  }else {
    console.log('Invalid format')
    return 'Invalid format'
  }
}
