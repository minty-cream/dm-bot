var env = require('node-env-file')
env(__dirname + '/config')

var bot = require(__dirname + '/libs/eris')
var testBotCmds = require(__dirname + '/test')
var interpretMessage = require(__dirname + '/libs/interpretMessage')

bot.on('ready', () => {
  console.log('Ready!')
})

bot.on('messageCreate', (msg) => {
  if (msg.content.indexOf('!') === 0) {
    let output = interpretMessage(msg)
    bot.createMessage(msg.channel.id, output)
  }
})

if (process.env.ENVIRONMENT === 'production' || process.env.ENVIRONMENT === 'staging') {
  bot.connect()
}else {
  testBotCmds()
}
